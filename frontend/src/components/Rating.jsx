import { useState } from 'react'
import './Rating.css'

function ModifiableRating({value, onRatingChange}){
    const [hoverValue, setHoverValue] = useState()
    const getHoverHandler = (value) => {
        return () => {
            setHoverValue(value)
        }
    }

    const stars = Array.from(function* () {
        const usedValue = hoverValue !== undefined ? hoverValue : value
        for (let i = 1; i <= 5; i++) {
            const src = (i <= usedValue)
                ? "../src/images/star-solid.svg"
                : "../src/images/star-regular.svg"
                yield (
                    <img 
                        onMouseLeave={() => setHoverValue(undefined)} 
                        onMouseOver={getHoverHandler(i)}
                        onClick={() => {onRatingChange(i)}}
                        className="star" key={i} src={src}></img>
                )
        }
    }())
    return (
        <div className="rating-container">{stars}</div>
    )
}

function Rating({ value }) {
    const stars = Array.from(function* () {
        for (let i = 1; i <= 5; i++) {
            const src = (i <= value)
                ? "../src/images/star-solid.svg"
                : "../src/images/star-regular.svg"
            yield <img className="star" key={i} src={src}></img>
        }
    }())
    return (
        <div className="rating-container">{stars}</div>
    )
}

export {ModifiableRating, Rating}