import './Popup.css'
import ratingsService from '../services/ratings.js'
import { HEART_UNICODE } from './RestaurantList'
import { useState } from 'react'
import {Rating, ModifiableRating} from './Rating.jsx'

const Popup = ({restaurant, avgRating, userRating, userId, token, closePopup, onRatingChange, isFavorite}) => {
    const listItems = [
        // (r) => {
        //     return ["name", r.name.fi]
        // },
        (r) => {
            return ["address", r.location.address.street_address]
        },
        (r) => {
            return ["description", r.description.intro]
        }
    ].map((func) => {
        const [key, value] = func(restaurant)
        return (
            <li key={key}><b>{key}:</b> {value}</li>
        )
    })

    async function createRating(value, userId, restaurantId, token){
        await ratingsService.createRating(value, userId, restaurantId, token)
    }

    async function updateRating(id, value){
        await ratingsService.updateRating(id, value, token)
    }

    async function handleRatingChange(value){
        try{
            if(typeof(userRating) === 'object'){
                await updateRating(userRating.id, value, token)
            }else{
                await createRating(value, userId, restaurant.id, token)
            }
            await onRatingChange()
        }catch(error){
            console.error('Rating change failed ', error)
        }
    }
    
    return (
        <div className="popup">
            <div className="popup-container">
                <span onClick={() => closePopup()} className="close">&times;</span>
                <h1>{restaurant.name.fi}{isFavorite && HEART_UNICODE}</h1>
                <div>Average rating: <Rating value={avgRating}></Rating></div>
                <div>Your rating: <ModifiableRating onRatingChange={handleRatingChange} value={userRating?.rating}></ModifiableRating></div>
                <ul>
                    {listItems}
                </ul>
            </div>
        </div>
    )
}

export default Popup